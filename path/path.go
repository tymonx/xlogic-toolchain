// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package path

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Abs is used for testing and mocking filepath.Abs.
var Abs = filepath.Abs // nolint: gochecknoglobals

// Rel is used for testing and mocking filepath.Rel.
var Rel = filepath.Rel // nolint: gochecknoglobals

// Path defines a single file path.
type Path string

// String returns file path as string.
func (p *Path) String() string {
	return string(*p)
}

// Clean returns cleaned file path.
func (p *Path) Clean() Path {
	return Path(filepath.Clean(p.String()))
}

// Extension returns file path extension.
func (p *Path) Extension() string {
	return filepath.Ext(p.String())
}

// Base returns base name.
func (p *Path) Base() string {
	return filepath.Base(p.String())
}

// Absolute returns absolute path.
func (p *Path) Absolute() Path {
	if ret, err := Abs(p.String()); err == nil {
		return Path(ret)
	}

	return *p
}

// Relative returns relative path.
func (p *Path) Relative(base string) Path {
	pBase := Path(base)

	if ret, err := Rel(string(pBase.Absolute()), string(p.Absolute())); err == nil {
		return Path(ret)
	}

	return *p
}

// File returns file name.
func (p *Path) File() Path {
	_, file := filepath.Split(p.String())

	return Path(file)
}

// Directory returns directory path.
func (p *Path) Directory() Path {
	dir, _ := filepath.Split(p.String())

	return Path(dir)
}

// IsEmpty returns true if path is empty (not set), otherwise it returns false.
func (p *Path) IsEmpty() bool {
	return *p == ""
}

// IsExist returns true if path exist, otherwise it returns false.
func (p *Path) IsExist() bool {
	_, err := os.Stat(p.String())

	return !os.IsNotExist(err)
}

// IsFile returns true if path is a file, otherwise it returns false.
func (p *Path) IsFile() bool {
	_, file := filepath.Split(p.String())

	return file != ""
}

// IsDirectory returns true if path is a directory, otherwise it returns false.
func (p *Path) IsDirectory() bool {
	dir, file := filepath.Split(p.String())

	return (file == "") && (dir != "")
}

// IsRelative returns true if path is relative, otherwise it returns false.
func (p *Path) IsRelative() bool {
	return !filepath.IsAbs(p.String())
}

// IsAbsolute returns true if path is absolute, otherwise it returns false.
func (p *Path) IsAbsolute() bool {
	return filepath.IsAbs(p.String())
}

// UnmarshalText decodes text to file path.
func (p *Path) UnmarshalText(data []byte) error {
	*p = Path(data)
	return nil
}

// MarshalText encodes file path to text.
func (p *Path) MarshalText() (data []byte, err error) {
	return []byte(p.String()), nil
}

// Load loads object fields from file.
func (p *Path) Load(object interface{}) (err error) {
	var data []byte

	if data, err = ioutil.ReadFile(p.String()); err != nil {
		return err
	}

	return json.Unmarshal(data, object)
}

// Dump dumps object fields to file.
func (p *Path) Dump(object interface{}) (err error) {
	var data []byte

	if data, err = json.Marshal(object); err != nil {
		return err
	}

	return ioutil.WriteFile(p.String(), data, 0600)
}
