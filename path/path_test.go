// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package path_test

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/tymonx/xlogic-toolchain/cerr"
	"gitlab.com/tymonx/xlogic-toolchain/path"
)

func TestPathString(test *testing.T) {
	want := "dir/file.v"

	p := path.Path(want)

	assert.Equal(test, want, p.String())
}

func TestPathClean(test *testing.T) {
	p := path.Path("dir/.//dir//file.b")

	assert.Equal(test, "dir/dir/file.b", string(p.Clean()))
}

func TestPathExtenstion(test *testing.T) {
	p := path.Path("dir/dir/file.c++")

	assert.Equal(test, ".c++", p.Extension())
}

func TestPathBase(test *testing.T) {
	p := path.Path("dir/dir/file.cpp")

	assert.Equal(test, "file.cpp", p.Base())
}

func TestPathAbsolute(test *testing.T) {
	dir, err := os.Getwd()

	assert.NoError(test, err)

	want := "dir/dir/file.cpp"

	p := path.Path(want)

	assert.Equal(test, filepath.Join(dir, want), string(p.Absolute()))
}

func TestPathRelative(test *testing.T) {
	_, file, _, ok := runtime.Caller(0)

	p := path.Path(file)

	dir := filepath.Dir(filepath.Dir(file))

	assert.True(test, ok)
	assert.Equal(test, "path/path_test.go", string(p.Relative(dir)))
}

func TestPathRelativeInvalid(test *testing.T) {
	defer func() {
		path.Rel = filepath.Rel
	}()

	path.Rel = func(string, string) (string, error) {
		return "", cerr.InvalidArgument
	}

	want := "dir/dir.m"

	p := path.Path(want)

	assert.Equal(test, want, string(p.Relative("")))
}

func TestPathAbsoluteInvalid(test *testing.T) {
	defer func() {
		path.Abs = filepath.Abs
	}()

	path.Abs = func(string) (string, error) {
		return "", cerr.InvalidArgument
	}

	want := `dir/dir/file.tcl`

	p := path.Path(want)

	assert.Equal(test, want, string(p.Absolute()))
}

func TestPathFile(test *testing.T) {
	p := path.Path("dir/dir/file.asm")

	assert.Equal(test, "file.asm", string(p.File()))
}

func TestPathDirectory(test *testing.T) {
	p := path.Path("dir/dir/file.ip")

	assert.Equal(test, "dir/dir/", string(p.Directory()))
}

func TestPathIsEmpty(test *testing.T) {
	var p path.Path

	assert.True(test, p.IsEmpty())
}

func TestPathIsExist(test *testing.T) {
	_, file, _, ok := runtime.Caller(0)

	p := path.Path(file)

	assert.True(test, ok)
	assert.True(test, p.IsExist())
}

func TestPathIsNotExist(test *testing.T) {
	var p path.Path

	assert.False(test, p.IsExist())
}

func TestPathIsFile(test *testing.T) {
	p := path.Path("dir/dir/file.txt")

	assert.True(test, p.IsFile())
	assert.False(test, p.IsDirectory())
}

func TestPathIsDirectory(test *testing.T) {
	p := path.Path("dir/dir/")

	assert.True(test, p.IsDirectory())
	assert.False(test, p.IsFile())
}

func TestPathIsRelative(test *testing.T) {
	p := path.Path("dir/dir/file.o")

	assert.True(test, p.IsRelative())
	assert.False(test, p.IsAbsolute())
}

func TestPathIsAbsolute(test *testing.T) {
	p := path.Path("/dir/file.o")

	assert.True(test, p.IsAbsolute())
	assert.False(test, p.IsRelative())
}

func TestPathUnmarshalJSON(test *testing.T) {
	var p path.Path

	want := "dir/file.e"

	assert.NoError(test, json.Unmarshal([]byte(` "`+want+`"  `), &p))
	assert.Equal(test, want, p.String())
}

func TestPathUnmarshalJSONNull(test *testing.T) {
	var p path.Path

	assert.NoError(test, json.Unmarshal([]byte(` null  `), &p))
	assert.Empty(test, p.String())
}

func TestPathUnmarshalJSONInvalid(test *testing.T) {
	var p path.Path

	assert.Error(test, json.Unmarshal([]byte(`3`), &p))
	assert.Empty(test, p.String())
}

func TestPathMarshalJSONInvalid(test *testing.T) {
	want := "dir/file.h"

	p := path.Path(want)

	data, err := json.Marshal(&p)

	assert.NoError(test, err)
	assert.Equal(test, []byte(`"`+want+`"`), data)
}

func TestPathDump(test *testing.T) {
	object := &struct {
		Value int `json:"value"`
	}{
		Value: 5,
	}

	file, err := ioutil.TempFile("", "tmp*")

	defer assert.NoError(test, os.Remove(file.Name()))
	defer assert.NoError(test, file.Close())

	assert.NoError(test, err)

	p := path.Path(file.Name())

	assert.NoError(test, p.Dump(object))
}

func TestPathDumpError(test *testing.T) {
	object := &struct {
		Chan chan struct{}
	}{}

	file, err := ioutil.TempFile("", "tmp*")

	defer assert.NoError(test, os.Remove(file.Name()))
	defer assert.NoError(test, file.Close())

	assert.NoError(test, err)

	p := path.Path(file.Name())

	assert.Error(test, p.Dump(object))
}

func TestPathLoad(test *testing.T) {
	object := &struct {
		Value int `json:"value"`
	}{}

	file, err := ioutil.TempFile("", "tmp*")

	defer assert.NoError(test, os.Remove(file.Name()))
	defer assert.NoError(test, file.Close())

	p := path.Path(file.Name())

	assert.NoError(test, err)
	assert.NoError(test, ioutil.WriteFile(file.Name(), []byte(`{ "value": 4 }`), 0600))
	assert.NoError(test, p.Load(&object))
	assert.Equal(test, 4, object.Value)
}

func TestPathLoadError(test *testing.T) {
	object := &struct {
		Value int `json:"value"`
	}{}

	p := path.Path("invalid")

	assert.Error(test, p.Load(&object))
	assert.Zero(test, object.Value)
}
