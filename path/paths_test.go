// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package path_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/tymonx/xlogic-toolchain/path"
)

func TestPathsUnmarsalJSONEmpty(test *testing.T) {
	var p path.Paths

	assert.NoError(test, json.Unmarshal([]byte(`[]`), &p))
	assert.Empty(test, p)
	assert.NotNil(test, p)
}

func TestPathsUnmarsalJSONNull(test *testing.T) {
	var p path.Paths

	assert.NoError(test, json.Unmarshal([]byte(`null`), &p))
	assert.Empty(test, p)
	assert.Nil(test, p)
}

func TestPathsMarsalJSON(test *testing.T) {
	p := path.Paths{
		"dir/file.c",
		"dir/file.v",
	}

	data, err := json.Marshal(&p)

	assert.NoError(test, err)
	assert.Equal(test, []byte(`["dir/file.c","dir/file.v"]`), data)
}
