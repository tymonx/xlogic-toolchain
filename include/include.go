// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package include

import (
	"encoding/json"

	"gitlab.com/tymonx/go-error/rterror"
	"gitlab.com/tymonx/xlogic-toolchain/path"
)

// Include defines a single include directory path.
type Include struct {
	Path path.Path `json:"path,omitempty"`
}

// Is returns true if path is an include directory, otherwise it returns false.
func Is(file path.Path) bool {
	return file.IsDirectory()
}

// New creates a new include object.
func New(dir path.Path) *Include {
	return &Include{
		Path: dir,
	}
}

// String returns path string.
func (i *Include) String() string {
	return i.Path.String()
}

// UnmarshalText decodes text to include directory path.
func (i *Include) UnmarshalText(data []byte) error {
	i.Path = path.Path(data)

	return nil
}

// UnmarshalJSON decodes JSON to include directory path.
func (i *Include) UnmarshalJSON(data []byte) error {
	switch data[0] {
	case '{':
	case '"':
		return i.UnmarshalText(data[1 : len(data)-1])
	case 'n':
		*i = Include{}
		return nil
	default:
		return rterror.New("invalid JSON type")
	}

	object := new(struct {
		Path path.Path `json:"path"`
	})

	if err := json.Unmarshal(data, object); err != nil {
		return rterror.New("error occurred while decoding JSON", err)
	}

	i.Path = object.Path

	return nil
}
