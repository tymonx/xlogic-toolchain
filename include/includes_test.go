// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package include_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/include"
)

func TestIncludesUnmarshalText(test *testing.T) {
	want := "dirA/dirB"

	var i include.Includes

	assert.NoError(test, json.Unmarshal([]byte(` "`+want+`"  `), &i))

	assert.Len(test, i, 1)
	assert.Equal(test, want, i[0].Path.String())
}

func TestIncludesUnmarshalJSON(test *testing.T) {
	var i include.Includes

	assert.NoError(test, json.Unmarshal([]byte(` ["dirB", { "path": "dirC/dirD"  }]`), &i))

	assert.Len(test, i, 2)
	assert.Equal(test, "dirB", i[0].Path.String())
	assert.Equal(test, "dirC/dirD", i[1].Path.String())
}

func TestIncludesUnmarshalJSONObject(test *testing.T) {
	want := "dirE/dirF"

	var i include.Includes

	assert.NoError(test, json.Unmarshal([]byte(` { "path": "`+want+`" }  `), &i))

	assert.Len(test, i, 1)
	assert.Equal(test, want, i[0].Path.String())
}

func TestIncludesUnmarshalJSONNull(test *testing.T) {
	var i include.Includes

	assert.NoError(test, json.Unmarshal([]byte(` null  `), &i))
	assert.Empty(test, i)
}

func TestIncludesUnmarshalJSONError(test *testing.T) {
	var i include.Includes

	assert.Error(test, json.Unmarshal([]byte(`[5]`), &i))
	assert.Empty(test, i)
}

func TestIncludesUnmarshalJSONObjectError(test *testing.T) {
	var i include.Includes

	assert.Error(test, json.Unmarshal([]byte(`{"path":5}`), &i))
	assert.Empty(test, i)
}

func TestIncludesUnmarshalJSONInvalid(test *testing.T) {
	var i include.Includes

	assert.Error(test, json.Unmarshal([]byte(`true`), &i))
	assert.Empty(test, i)
}
