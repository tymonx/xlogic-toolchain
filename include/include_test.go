// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package include_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/include"
	"gitlab.com/tymonx/xlogic-toolchain/path"
)

func TestIncludeNew(test *testing.T) {
	want := "dir/dir/dir"

	i := include.New(path.Path(want))

	assert.NotNil(test, i)
	assert.Equal(test, want, i.Path.String())
}

func TestIncludeString(test *testing.T) {
	want := "dirA"

	assert.NotNil(test, want, include.New(path.Path(want)).String())
}

func TestIncludeIs(test *testing.T) {
	assert.True(test, include.Is("dir/"))
	assert.False(test, include.Is("file.c"))
}

func TestIncludeUnmarshalText(test *testing.T) {
	want := "dirB"

	i := new(include.Include)

	assert.NoError(test, json.Unmarshal([]byte(`  "`+want+`"  `), i))
	assert.Equal(test, want, i.Path.String())
}

func TestIncludeUnmarshalJSONObject(test *testing.T) {
	want := "dirC"

	i := new(include.Include)

	assert.NoError(test, json.Unmarshal([]byte(` { "path": "`+want+`" }  `), i))
	assert.Equal(test, want, i.Path.String())
}

func TestIncludeUnmarshalJSONNull(test *testing.T) {
	i := new(include.Include)

	assert.NoError(test, json.Unmarshal([]byte(` null  `), i))
	assert.Empty(test, i.Path.String())
}

func TestIncludeUnmarshalJSONError(test *testing.T) {
	i := new(include.Include)

	assert.Error(test, json.Unmarshal([]byte(`{"path":5}`), i))
	assert.Empty(test, i.Path.String())
}

func TestIncludeUnmarshalJSONInvalid(test *testing.T) {
	i := new(include.Include)

	assert.Error(test, json.Unmarshal([]byte(`true`), i))
	assert.Empty(test, i.Path.String())
}
