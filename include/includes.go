// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package include

import (
	"encoding/json"

	"gitlab.com/tymonx/go-error/rterror"
	"gitlab.com/tymonx/xlogic-toolchain/path"
)

// Includes defines a list of include directory paths.
type Includes []Include

// UnmarshalText decodes text to include directory path.
func (i *Includes) UnmarshalText(data []byte) error {
	*i = Includes{*New(path.Path(data))}

	return nil
}

// UnmarshalJSON decodes JSON to include directory path.
func (i *Includes) UnmarshalJSON(data []byte) error {
	switch data[0] {
	case '[':
	case '{':
		object := new(Include)

		if err := json.Unmarshal(data, object); err != nil {
			return err
		}

		*i = Includes{*object}

		return nil
	case '"':
		return i.UnmarshalText(data[1 : len(data)-1])
	case 'n':
		*i = Includes{}
		return nil
	default:
		return rterror.New("invalid JSON type")
	}

	includes := []Include{}

	if err := json.Unmarshal(data, &includes); err != nil {
		return rterror.New("error occurred while decoding JSON", err)
	}

	*i = includes

	return nil
}
