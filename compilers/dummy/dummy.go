// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dummy

import (
	"math"

	"gitlab.com/tymonx/xlogic-toolchain/compiler"
	"gitlab.com/tymonx/xlogic-toolchain/file"
)

// Dummy defines dummy compiler.
type Dummy struct{}

// Priority returns compiler priority.
func (*Dummy) Priority() int {
	return math.MinInt32
}

// CanExecute returns true if compiler can execute provided file, otherwise it returns false.
func (*Dummy) CanExecute(*file.File) bool {
	return true
}

// Execute executes compiler.
func (*Dummy) Execute(*file.File) error {
	return nil
}

func init() { // nolint: gochecknoinits
	if err := compiler.Add("dummy", new(Dummy)); err != nil {
		panic(err)
	}
}
