// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package factory_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/cerr"
	"gitlab.com/tymonx/xlogic-toolchain/factory"
)

func TestFactoryNew(test *testing.T) {
	assert.NotNil(test, factory.New())
}

func TestFactoryRegistry(test *testing.T) {
	constructor := func(...interface{}) (interface{}, error) {
		return new(struct{}), nil
	}

	assert.NoError(test, factory.New().Registry("type", constructor))
}

func TestFactoryRegistryGlobal(test *testing.T) {
	constructor := func(...interface{}) (interface{}, error) {
		return new(struct{}), nil
	}

	assert.NoError(test, factory.Registry("type", constructor))
}

func TestFactoryRegistryAlreadyRegistered(test *testing.T) {
	f := factory.New()

	constructor := func(...interface{}) (interface{}, error) {
		return new(struct{}), nil
	}

	assert.NoError(test, f.Registry("type", constructor))
	assert.Error(test, f.Registry("type", constructor))
}

func TestFactoryRegistryInvalidConstructor(test *testing.T) {
	assert.Error(test, factory.New().Registry("type", nil))
}

func TestFactoryCreate(test *testing.T) {
	f := factory.New()

	constructor := func(...interface{}) (interface{}, error) {
		return new(struct{}), nil
	}

	assert.NoError(test, f.Registry("type", constructor))

	object, err := f.Create("type")

	assert.NoError(test, err)
	assert.NotNil(test, object)
}

func TestFactoryCreateGlobal(test *testing.T) {
	constructor := func(...interface{}) (interface{}, error) {
		return new(struct{}), nil
	}

	assert.NoError(test, factory.Registry("type-2", constructor))

	object, err := factory.Create("type-2")

	assert.NoError(test, err)
	assert.NotNil(test, object)
}

func TestFactoryCreateNotRegistered(test *testing.T) {
	object, err := factory.New().Create("type")

	assert.Error(test, err)
	assert.Nil(test, object)
}

func TestFactoryCreateError(test *testing.T) {
	f := factory.New()

	constructor := func(...interface{}) (interface{}, error) {
		return nil, cerr.InvalidValue
	}

	assert.NoError(test, f.Registry("type", constructor))

	object, err := f.Create("type")

	assert.Error(test, err)
	assert.Nil(test, object)
}

func TestFactoryCreateNotCreated(test *testing.T) {
	f := factory.New()

	constructor := func(...interface{}) (interface{}, error) {
		return nil, nil
	}

	assert.NoError(test, f.Registry("type", constructor))

	object, err := f.Create("type")

	assert.Error(test, err)
	assert.Nil(test, object)
}
