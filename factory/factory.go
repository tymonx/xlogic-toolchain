// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package factory

import (
	"sync"

	"gitlab.com/tymonx/xlogic-toolchain/cerr"
)

// Constructor defines a custom constructor function for creating objects.
type Constructor func(arguments ...interface{}) (object interface{}, err error)

// Factory defines a factory instance that can create registered object types.
type Factory struct {
	constructors map[string]Constructor
}

var gInstance *Factory  // nolint: gochecknoglobals
var gMutex sync.RWMutex // nolint: gochecknoglobals
var gOnce sync.Once     // nolint: gochecknoglobals

// New creates a new factory instance.
func New() *Factory {
	return &Factory{
		constructors: make(map[string]Constructor),
	}
}

// Registry registries a new constructor with a given unique id to factory.
func (f *Factory) Registry(name string, constructor Constructor) error {
	if constructor == nil {
		return cerr.InvalidArgument
	}

	if _, ok := f.constructors[name]; ok {
		return cerr.InvalidArgument
	}

	f.constructors[name] = constructor

	return nil
}

// Create creates a new object based on given name type.
func (f *Factory) Create(name string, arguments ...interface{}) (object interface{}, err error) {
	var constructor Constructor

	var ok bool

	if constructor, ok = f.constructors[name]; !ok {
		return nil, cerr.InvalidArgument
	}

	if object, err = constructor(arguments...); err != nil {
		return nil, cerr.InvalidArgument
	}

	if object == nil {
		return nil, cerr.InvalidArgument
	}

	return object, nil
}

// Registry registries a new constructor with a given unique id to factory.
func Registry(name string, constructor Constructor) error {
	gMutex.Lock()
	defer gMutex.Unlock()

	return get().Registry(name, constructor)
}

// Create creates a new object based on given name type.
func Create(name string, arguments ...interface{}) (object interface{}, err error) {
	gMutex.RLock()
	defer gMutex.RUnlock()

	return get().Create(name, arguments...)
}

// get returns global factory instance.
func get() *Factory {
	gOnce.Do(func() {
		gInstance = New()
	})

	return gInstance
}
