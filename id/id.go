// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package id

import (
	crand "crypto/rand"
	"encoding/hex"
	"io"
	"math/rand"
	"sync"
	"time"
)

const (
	length = 16
)

// Reader is used only in tests to mock the rand.Reader function.
var Reader = crand.Reader // nolint: gochecknoglobals

var gOnce sync.Once   // nolint: gochecknoglobals
var gMutex sync.Mutex // nolint: gochecknoglobals
var gRand *rand.Rand  // nolint: gochecknoglobals

// ID defines unique ID.
type ID string

// New returns a new global unique ID.
func New() ID {
	var bytes [length]byte

	if _, err := io.ReadFull(Reader, bytes[:]); err != nil {
		return ID(generateSoftly())
	}

	return ID(convertBytesToString(bytes[:]))
}

// String returns ID as a string.
func (i *ID) String() string {
	return string(*i)
}

// generateSoftly softly generates a global unique ID. Possibility for conflicts.
// Consider to use the Generate function instead of using this function for generating global unique IDs.
func generateSoftly() string {
	gOnce.Do(func() {
		gRand = rand.New(rand.NewSource(time.Now().UnixNano()))
	})

	var bytes [length]byte

	func() {
		gMutex.Lock()
		defer gMutex.Unlock()

		for i := range bytes {
			bytes[i] = byte(gRand.Int())
		}
	}()

	return convertBytesToString(bytes[:])
}

// convertBytesToString converts bytes to UUID4 format string.
func convertBytesToString(bytes []byte) string {
	// UUID4 byte format
	bytes[6] = 0x40 | (bytes[6] & 0x0F)
	bytes[8] = 0x80 | (bytes[8] & 0x3F)

	str := hex.EncodeToString(bytes)

	// UUID4 string format: xxxxxxxx-xxxx-4xxx-[89ab]xxx-xxxxxxxxxxxx
	return str[0:8] + "-" + str[8:12] + "-" + str[12:16] + "-" + str[16:20] + "-" + str[20:32]
}
