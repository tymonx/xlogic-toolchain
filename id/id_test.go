// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package id_test

import (
	"crypto/rand"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/id"
)

func TestIDGenerate(test *testing.T) {
	want := 1024

	ids := make(map[id.ID]bool)

	uuid4Regexp := regexp.MustCompile(UUID4Schema)

	for i := 0; i < want; i++ {
		value := id.New()

		assert.Regexp(test, uuid4Regexp, value.String())

		ids[value] = true
	}

	assert.Len(test, ids, want)
}

func TestIDGenerateError(test *testing.T) {
	defer func() {
		id.Reader = rand.Reader
	}()

	id.Reader = new(mockReader)

	assert.Regexp(test, regexp.MustCompile(UUID4Schema), string(id.New()))
}
