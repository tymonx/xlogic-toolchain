// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package compiler

import (
	"sort"
	"sync"

	"gitlab.com/tymonx/go-error/rterror"
	"gitlab.com/tymonx/xlogic-toolchain/file"
)

// Compilers defines a map of compilers.
type Compilers struct {
	items map[string]Compiler
}

var gCompilers *Compilers // nolint: gochecknoglobals
var gOnce sync.Once       // nolint: gochecknoglobals
var gMutex sync.RWMutex   // nolint: gochecknoglobals

// Add adds new compiler.
func (c *Compilers) Add(name string, item Compiler) error {
	if item == nil {
		return rterror.New("compiler cannot be nil")
	}

	if _, ok := c.items[name]; ok {
		return rterror.New("compiler already exist", name)
	}

	c.items[name] = item

	return nil
}

// Execute executes compiler.
func (c *Compilers) Execute(f *file.File) (err error) {
	items := make([]Compiler, 0, len(c.items))

	for _, item := range c.items {
		items = append(items, item)
	}

	sort.Slice(items, func(i, j int) bool {
		return items[i].Priority() > items[j].Priority()
	})

	for _, item := range items {
		if item.CanExecute(f) {
			return item.Execute(f)
		}
	}

	return rterror.New("cannot execute", f)
}

// Add adds new compiler.
func Add(name string, item Compiler) (err error) {
	gMutex.Lock()
	defer gMutex.Unlock()

	return getInstance().Add(name, item)
}

// Execute executes compiler.
func Execute(f *file.File) (err error) {
	gMutex.RLock()
	defer gMutex.RUnlock()

	return getInstance().Execute(f)
}

func getInstance() *Compilers {
	gOnce.Do(func() {
		gCompilers = &Compilers{
			items: make(map[string]Compiler),
		}
	})

	return gCompilers
}
