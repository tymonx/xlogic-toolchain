// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package arguments

import (
	"errors"
	"os"
	"path/filepath"

	"github.com/alexflint/go-arg"
	"gitlab.com/tymonx/go-error/rterror"
	"gitlab.com/tymonx/xlogic-toolchain/define"
	"gitlab.com/tymonx/xlogic-toolchain/file"
	"gitlab.com/tymonx/xlogic-toolchain/header"
	"gitlab.com/tymonx/xlogic-toolchain/include"
	"gitlab.com/tymonx/xlogic-toolchain/mode"
	"gitlab.com/tymonx/xlogic-toolchain/path"
	"gitlab.com/tymonx/xlogic-toolchain/undefine"
)

// These constants define default values for compiler arguments.
const (
	DefaultOutput      = "output.json"
	DefaultMode        = mode.Executable
	DefaultProgramName = "xlogic-compiler"
)

// Arguments defines compiler arguments.
type Arguments struct {
	Append    bool                `arg:"-a,--append" help:"Append file"`
	Mode      mode.Mode           `arg:"-m,--mode" help:"Compiler mode: object, archive, library, executable" default:"executable"`
	Output    path.Path           `arg:"-o,--output" help:"Place output in file" default:"output.json" placeholder:"FILE"`
	Compilers []string            `arg:"-c,--compiler" help:"Compiler name or path" placeholder:"COMPILER"`
	Defines   []define.Define     `arg:"-D,--define,separate" help:"Set preprocessor define" placeholder:"NAME[=VALUE]"`
	Undefines []undefine.Undefine `arg:"-U,--undefine,separate" help:"Unset preprocessor define" placeholder:"NAME"`
	Headers   []header.Header     `arg:"-H,--header,separate" help:"Include header file" placeholder:"FILE"`
	Includes  []include.Include   `arg:"-I,--include,separate" help:"Directory to search for includes" placeholder:"DIR"`
	Files     []file.File         `arg:"positional" help:"Input files" placeholder:"FILE"`
}

// New creates a new compiler arguments object.
func New() *Arguments {
	return &Arguments{
		Mode:   DefaultMode,
		Output: DefaultOutput,
	}
}

// Parse parses arguments.
func (a *Arguments) Parse(args ...string) (err error) {
	var parser *arg.Parser

	config := arg.Config{}

	if len(args) == 0 {
		args = os.Args[1:]
		config.Program = filepath.Base(os.Args[0])
	} else {
		config.Program = DefaultProgramName
	}

	if parser, err = arg.NewParser(arg.Config{}, a); err != nil {
		return rterror.New("error occurred while creating parser", err)
	}

	if err = parser.Parse(args); errors.Is(err, arg.ErrHelp) {
		parser.WriteHelp(os.Stdout)
	} else if err != nil {
		return rterror.New("error occurred while parsing arguments", err)
	}

	return nil
}
