// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"gitlab.com/tymonx/xlogic-toolchain/arguments"
	"gitlab.com/tymonx/xlogic-toolchain/compiler"
	"gitlab.com/tymonx/xlogic-toolchain/file"
	"gitlab.com/tymonx/xlogic-toolchain/file/kind"
	"gitlab.com/tymonx/xlogic-toolchain/mode"

	_ "gitlab.com/tymonx/xlogic-toolchain/compilers"
)

func main() {
	args := arguments.New()

	if err := args.Parse(); err != nil {
		panic(err)
	}

	output := file.New(args.Output)

	switch args.Mode {
	case mode.Object:
		output.Kind = kind.Object
	case mode.Archive:
		output.Kind = kind.Archive
	case mode.Library:
		output.Kind = kind.Library
	case mode.Executable:
		output.Kind = kind.Executable
	}

	if args.Append {
		if err := output.Load(args.Output); err != nil {
			panic(err)
		}
	}

	output.Files = append(output.Files, args.Files...)
	output.Defines = append(output.Defines, args.Defines...)
	output.Headers = append(output.Headers, args.Headers...)
	output.Includes = append(output.Includes, args.Includes...)
	output.Undefines = append(output.Undefines, args.Undefines...)
	output.Compilers = append(output.Compilers, args.Compilers...)

	if err := compiler.Execute(output); err != nil {
		panic(err)
	}

	if err := output.Dump(args.Output); err != nil {
		panic(err)
	}
}
