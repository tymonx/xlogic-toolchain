// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mode

import (
	"strings"

	"gitlab.com/tymonx/go-error/rterror"
)

// These constants define compiler modes.
const (
	Executable Mode = 0
	Library    Mode = 1
	Archive    Mode = 2
	Object     Mode = 3
)

var gFromString = map[string]Mode{ // nolint: gochecknoglobals
	"":           Executable,
	"link":       Executable,
	"create":     Executable,
	"default":    Executable,
	"executable": Executable,
	"library":    Library,
	"shared":     Library,
	"archive":    Archive,
	"static":     Archive,
	"object":     Object,
	"compile":    Object,
}

var gToString = map[Mode]string{ // nolint: gochecknoglobals
	Executable: "executable",
	Library:    "library",
	Archive:    "archive",
	Object:     "object",
}

// Mode defines compiler mode like creating executable, library, archive or object.
type Mode int

// String returns compiler mode as a string.
func (m Mode) String() string {
	return gToString[m]
}

// UnmarshalText decodes text to compiler mode.
func (m *Mode) UnmarshalText(data []byte) error {
	if value, ok := gFromString[strings.TrimSpace(strings.ToLower(string(data)))]; ok {
		*m = value
		return nil
	}

	return rterror.New("error occurred while decoding text to compiler mode", string(data))
}

// MarshalText encodes compiler mode to text.
func (m Mode) MarshalText() (data []byte, err error) {
	return []byte(m.String()), nil
}
