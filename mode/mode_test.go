// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mode_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/mode"
)

func TestModeString(test *testing.T) {
	assert.Equal(test, "object", mode.Object.String())
	assert.Equal(test, "archive", mode.Archive.String())
	assert.Equal(test, "library", mode.Library.String())
	assert.Equal(test, "executable", mode.Executable.String())
}

func TestModeUnmarshalText(test *testing.T) {
	var m mode.Mode

	assert.NoError(test, json.Unmarshal([]byte(` " object" `), &m))
	assert.Equal(test, mode.Object, m)
}

func TestModeUnmarshalTextError(test *testing.T) {
	var m mode.Mode

	assert.Error(test, json.Unmarshal([]byte(`"invalid"`), &m))
	assert.Equal(test, mode.Executable, m)
}

func TestModeMarshalText(test *testing.T) {
	data, err := json.Marshal(mode.Archive)

	assert.NoError(test, err)
	assert.Equal(test, []byte(`"archive"`), data)
}
