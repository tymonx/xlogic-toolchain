// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package undefine

import (
	"encoding/json"

	"gitlab.com/tymonx/go-error/rterror"
)

// Undefines undefines a list of undefines.
type Undefines []Undefine

// UnmarshalText decodes text to undefine.
func (u *Undefines) UnmarshalText(data []byte) error {
	*u = Undefines{*New(string(data))}

	return nil
}

// UnmarshalJSON decodes JSON to undefine.
func (u *Undefines) UnmarshalJSON(data []byte) error {
	switch data[0] {
	case '[':
	case '{':
		object := new(Undefine)

		if err := json.Unmarshal(data, object); err != nil {
			return err
		}

		*u = Undefines{*object}

		return nil
	case '"':
		return u.UnmarshalText(data[1 : len(data)-1])
	case 'n':
		*u = Undefines{}
		return nil
	default:
		return rterror.New("invalid JSON type")
	}

	undefines := []Undefine{}

	if err := json.Unmarshal(data, &undefines); err != nil {
		return rterror.New("error occurred while decoding JSON", err)
	}

	*u = undefines

	return nil
}
