// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implieu.
// See the License for the specific language governing permissions and
// limitations under the License.

package undefine_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/undefine"
)

func TestUndefineNew(test *testing.T) {
	want := "Name"

	u := undefine.New(want)

	assert.NotNil(test, u)
	assert.Equal(test, "Name", u.Name)
	assert.Equal(test, want, u.String())
}

func TestUndefineString(test *testing.T) {
	want := "NAME"

	assert.NotNil(test, want, undefine.New(want).String())
}

func TestUndefineUnmarshalText(test *testing.T) {
	want := "MYDEF"

	u := new(undefine.Undefine)

	assert.NoError(test, json.Unmarshal([]byte(`  "  `+want+` "  `), u))

	assert.Equal(test, want, u.String())
	assert.Equal(test, want, u.Name)
}

func TestUndefineUnmarshalJSONObject(test *testing.T) {
	want := "DEFINE"

	u := new(undefine.Undefine)

	assert.NoError(test, json.Unmarshal([]byte(` { "name": "`+want+`" }  `), u))

	assert.Equal(test, want, u.String())
	assert.Equal(test, want, u.Name)
}

func TestUndefineUnmarshalJSONNull(test *testing.T) {
	u := new(undefine.Undefine)

	assert.NoError(test, json.Unmarshal([]byte(` null  `), u))

	assert.Empty(test, u.String())
	assert.Empty(test, u.Name)
}

func TestUndefineUnmarshalJSONError(test *testing.T) {
	u := new(undefine.Undefine)

	assert.Error(test, json.Unmarshal([]byte(`{"name":5}`), u))

	assert.Empty(test, u.String())
	assert.Empty(test, u.Name)
}

func TestUndefineUnmarshalJSONInvalid(test *testing.T) {
	u := new(undefine.Undefine)

	assert.Error(test, json.Unmarshal([]byte(`true`), u))

	assert.Empty(test, u.String())
	assert.Empty(test, u.Name)
}
