// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package undefine_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/undefine"
)

func TestUndefinesUnmarshalText(test *testing.T) {
	want := "DEFF"

	var u undefine.Undefines

	assert.NoError(test, json.Unmarshal([]byte(` "`+want+`"  `), &u))

	assert.Len(test, u, 1)
	assert.Equal(test, want, u[0].Name)
	assert.Equal(test, want, u[0].String())
}

func TestUndefinesUnmarshalJSON(test *testing.T) {
	var u undefine.Undefines

	assert.NoError(test, json.Unmarshal([]byte(` ["A", { "name": "B"  }]`), &u))

	assert.Len(test, u, 2)

	assert.Equal(test, "A", u[0].Name)
	assert.Equal(test, "A", u[0].String())

	assert.Equal(test, "B", u[1].Name)
	assert.Equal(test, "B", u[1].String())
}

func TestUndefinesUnmarshalJSONObject(test *testing.T) {
	want := "C"

	var u undefine.Undefines

	assert.NoError(test, json.Unmarshal([]byte(` { "name": "`+want+`" }  `), &u))

	assert.Len(test, u, 1)
	assert.Equal(test, want, u[0].String())
	assert.Equal(test, want, u[0].Name)
}

func TestUndefinesUnmarshalJSONNull(test *testing.T) {
	var u undefine.Undefines

	assert.NoError(test, json.Unmarshal([]byte(` null  `), &u))
	assert.Empty(test, u)
}

func TestUndefinesUnmarshalJSONError(test *testing.T) {
	var u undefine.Undefines

	assert.Error(test, json.Unmarshal([]byte(`[5]`), &u))
	assert.Empty(test, u)
}

func TestUndefinesUnmarshalJSONObjectError(test *testing.T) {
	var u undefine.Undefines

	assert.Error(test, json.Unmarshal([]byte(`{"name":5}`), &u))
	assert.Empty(test, u)
}

func TestUndefinesUnmarshalJSONInvalid(test *testing.T) {
	var u undefine.Undefines

	assert.Error(test, json.Unmarshal([]byte(`true`), &u))
	assert.Empty(test, u)
}
