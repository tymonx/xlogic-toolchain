// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package undefine

import (
	"encoding/json"
	"strings"

	"gitlab.com/tymonx/go-error/rterror"
)

// Undefine undefines undefine.
type Undefine struct {
	Name string `json:"name,omitempty"`
}

// New creates a new undefine object.
func New(name string) *Undefine {
	return &Undefine{
		Name: strings.TrimSpace(name),
	}
}

// String returns undefine as string.
func (u *Undefine) String() string {
	return u.Name
}

// UnmarshalText decodes text to undefine.
func (u *Undefine) UnmarshalText(data []byte) error {
	u.Name = strings.TrimSpace(string(data))
	return nil
}

// UnmarshalJSON decodes JSON to undefine.
func (u *Undefine) UnmarshalJSON(data []byte) error {
	switch data[0] {
	case '{':
	case '"':
		return u.UnmarshalText(data[1 : len(data)-1])
	case 'n':
		*u = Undefine{}
		return nil
	default:
		return rterror.New("invalid JSON type")
	}

	object := new(struct {
		Name string `json:"name"`
	})

	if err := json.Unmarshal(data, object); err != nil {
		return rterror.New("error occurred while decoding JSON", err)
	}

	u.Name = strings.TrimSpace(object.Name)

	return nil
}
