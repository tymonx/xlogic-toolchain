// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package file_test

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/go-error/rterror"
	"gitlab.com/tymonx/xlogic-toolchain/file"
	"gitlab.com/tymonx/xlogic-toolchain/file/kind"
	"gitlab.com/tymonx/xlogic-toolchain/path"
)

func TestFileNew(test *testing.T) {
	want := "dir/file.sv"

	f := file.New(path.Path(want))

	assert.NotNil(test, f)
	assert.Equal(test, want, f.Path.String())
	assert.Equal(test, kind.SystemVerilog, f.Kind)
	assert.NotEmpty(test, f.ID)
}

func TestFileLoad(test *testing.T) { // nolint: dupl
	h, err := ioutil.TempFile("", "*.c")

	defer assert.NoError(test, os.Remove(h.Name()))
	defer assert.NoError(test, h.Close())

	f := file.New("")

	assert.NoError(test, err)
	assert.NoError(test, ioutil.WriteFile(h.Name(), []byte(`{}`), 0600))
	assert.NoError(test, f.Load(path.Path(h.Name())))

	assert.Equal(test, h.Name(), f.Path.String())
	assert.Equal(test, kind.C, f.Kind)
	assert.NotEmpty(test, f.ID)
}

func TestFileLoadJSON(test *testing.T) {
	h, err := ioutil.TempFile("", "*.json")

	defer assert.NoError(test, os.Remove(h.Name()))
	defer assert.NoError(test, h.Close())

	f := file.New("")

	assert.NoError(test, err)
	assert.NoError(test, ioutil.WriteFile(h.Name(), []byte(`{"id": "id", "kind": "object", "path": "file.o"}`), 0600))
	assert.NoError(test, f.Load(path.Path(h.Name())))

	assert.Equal(test, "file.o", f.Path.String())
	assert.Equal(test, kind.Object, f.Kind)
	assert.Equal(test, "id", f.ID.String())
}

func TestFileLoadJSONError(test *testing.T) {
	h, err := ioutil.TempFile("", "*.json")

	defer assert.NoError(test, os.Remove(h.Name()))
	defer assert.NoError(test, h.Close())

	f := file.New("")

	assert.NoError(test, err)
	assert.NoError(test, ioutil.WriteFile(h.Name(), []byte(`{"id": true}`), 0600))
	assert.Error(test, f.Load(path.Path(h.Name())))

	assert.Empty(test, f.Path)
	assert.Equal(test, kind.Unknown, f.Kind)
	assert.NotEmpty(test, f.ID)
}

func TestFileLoadReadError(test *testing.T) {
	f := file.New("")

	assert.Error(test, f.Load("invalid"))

	assert.Empty(test, f.Path)
	assert.Equal(test, kind.Unknown, f.Kind)
	assert.NotEmpty(test, f.ID)
}

func TestFileLoadUnmarshalError(test *testing.T) { // nolint: dupl
	h, err := ioutil.TempFile("", "*.v")

	defer assert.NoError(test, os.Remove(h.Name()))
	defer assert.NoError(test, h.Close())

	f := file.New("")

	assert.NoError(test, err)
	assert.NoError(test, ioutil.WriteFile(h.Name(), []byte(`{ "data": 5 }`), 0600))
	assert.NoError(test, f.Load(path.Path(h.Name())))

	assert.Equal(test, h.Name(), f.Path.String())
	assert.Equal(test, kind.Verilog, f.Kind)
	assert.NotEmpty(test, f.ID)
}

func TestFileDump(test *testing.T) { // nolint: dupl
	h, err := ioutil.TempFile("", "*.json")

	defer assert.NoError(test, os.Remove(h.Name()))
	defer assert.NoError(test, h.Close())

	f := file.New("file.o")

	assert.NoError(test, err)
	assert.NoError(test, f.Dump(path.Path(h.Name())))

	object := &struct {
		ID   string `json:"id"`
		Path string `json:"path"`
		Kind string `json:"kind"`
	}{}

	data, err := ioutil.ReadFile(h.Name())

	assert.NotNil(test, data)
	assert.NoError(test, err)
	assert.NoError(test, json.Unmarshal(data, object))
	assert.Equal(test, "file.o", object.Path)
	assert.Equal(test, "object", object.Kind)
	assert.NotEmpty(test, object.ID)
}

func TestFileDumpError(test *testing.T) { // nolint: dupl
	h, err := ioutil.TempFile("", "*.json")

	defer assert.NoError(test, os.Remove(h.Name()))
	defer assert.NoError(test, h.Close())

	f := file.New("file.o")

	assert.NoError(test, err)
	assert.NoError(test, f.Dump(path.Path(h.Name())))

	object := &struct {
		ID   string `json:"id"`
		Path string `json:"path"`
		Kind string `json:"kind"`
	}{}

	data, err := ioutil.ReadFile(h.Name())

	assert.NotNil(test, data)
	assert.NoError(test, err)
	assert.NoError(test, json.Unmarshal(data, object))
	assert.Equal(test, "file.o", object.Path)
	assert.Equal(test, "object", object.Kind)
	assert.NotEmpty(test, object.ID)
}

func TestFileDumpWriteError(test *testing.T) {
	d, err := ioutil.TempDir("", "")

	defer assert.NoError(test, os.Remove(d))

	f := file.New("file.o")

	assert.NoError(test, err)
	assert.Error(test, f.Dump(path.Path(d+"/")))
}

func TestFileDumpMarshalError(test *testing.T) {
	defer func() {
		file.Marshal = json.Marshal
	}()

	file.Marshal = func(interface{}) ([]byte, error) {
		return nil, rterror.New("error")
	}

	assert.Error(test, file.New("").Dump(""))
}

func TestFileUnmarshalText(test *testing.T) {
	h, err := ioutil.TempFile("", "*.json")

	defer assert.NoError(test, os.Remove(h.Name()))
	defer assert.NoError(test, h.Close())

	var f file.File

	assert.NoError(test, err)
	assert.NoError(test, ioutil.WriteFile(h.Name(), []byte(`{"path": "file.o"}`), 0600))
	assert.NoError(test, json.Unmarshal([]byte(`"`+h.Name()+`"`), &f))
	assert.Equal(test, "file.o", f.Path.String())
}

func TestFileUnmarshalNull(test *testing.T) {
	var f file.File

	assert.NoError(test, json.Unmarshal([]byte(` null `), &f))
	assert.Empty(test, f)
}

func TestFileUnmarshalInvalid(test *testing.T) {
	var f file.File

	assert.Error(test, json.Unmarshal([]byte(` true `), &f))
	assert.Empty(test, f)
}
