// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package file

import (
	"encoding/json"

	"gitlab.com/tymonx/go-error/rterror"
	"gitlab.com/tymonx/xlogic-toolchain/path"
)

// Files defines a list of generic objects that describes input or generated output files.
type Files []File

// UnmarshalText decodes text to file path.
func (f *Files) UnmarshalText(data []byte) error {
	object := new(File)

	if err := object.Load(path.Path(data)); err != nil {
		return rterror.New("error occurred while decoding text", string(data), err)
	}

	*f = Files{*object}

	return nil
}

// UnmarshalJSON decodes JSON to file path.
func (f *Files) UnmarshalJSON(data []byte) error {
	switch data[0] {
	case '[':
	case '{':
		object := new(File)

		if err := json.Unmarshal(data, object); err != nil {
			return err
		}

		*f = Files{*object}

		return nil
	case '"':
		return f.UnmarshalText(data[1 : len(data)-1])
	case 'n':
		*f = Files{}
		return nil
	default:
		return rterror.New("invalid JSON type")
	}

	files := []File{}

	if err := json.Unmarshal(data, &files); err != nil {
		return rterror.New("error occurred while decoding JSON", err)
	}

	*f = files

	return nil
}
