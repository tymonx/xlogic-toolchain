// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, foftware
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package file_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/file"
	"gitlab.com/tymonx/xlogic-toolchain/file/kind"
)

func TestFilesUnmarshalText(test *testing.T) {
	want := "dir/file.c++"

	var f file.Files

	assert.NoError(test, json.Unmarshal([]byte(` "`+want+`"  `), &f))

	assert.Len(test, f, 1)
	assert.Equal(test, kind.Cxx, f[0].Kind)
	assert.Equal(test, "c++", f[0].Kind.String())
	assert.Equal(test, want, f[0].Path.String())
}

func TestFilesUnmarshalTextError(test *testing.T) {
	var f file.Files

	assert.Error(test, json.Unmarshal([]byte(` "invalid"  `), &f))
	assert.Empty(test, f)
}

func TestFilesUnmarshalJSON(test *testing.T) {
	var f file.Files

	assert.NoError(test, json.Unmarshal([]byte(` ["dir/file.c", { "path": "file.tcl"  }]`), &f))

	assert.Len(test, f, 2)

	assert.Equal(test, kind.C, f[0].Kind)
	assert.Equal(test, "c", f[0].Kind.String())
	assert.Equal(test, "dir/file.c", f[0].Path.String())

	assert.Equal(test, kind.TCL, f[1].Kind)
	assert.Equal(test, "tcl", f[1].Kind.String())
	assert.Equal(test, "file.tcl", f[1].Path.String())
}

func TestFilesUnmarshalJSONObject(test *testing.T) {
	want := "dir/file.json"

	var f file.Files

	assert.NoError(test, json.Unmarshal([]byte(` { "path": "`+want+`" }  `), &f))

	assert.Len(test, f, 1)
	assert.Equal(test, want, f[0].Path.String())
	assert.Equal(test, kind.JSON, f[0].Kind)
	assert.Equal(test, "json", f[0].Kind.String())
}

func TestFilesUnmarshalJSONNull(test *testing.T) {
	var f file.Files

	assert.NoError(test, json.Unmarshal([]byte(` null  `), &f))
	assert.Empty(test, f)
}

func TestFilesUnmarshalJSONError(test *testing.T) {
	var f file.Files

	assert.Error(test, json.Unmarshal([]byte(`[5]`), &f))
	assert.Empty(test, f)
}

func TestFilesUnmarshalJSONObjectError(test *testing.T) {
	var f file.Files

	assert.Error(test, json.Unmarshal([]byte(`{"path":5}`), &f))
	assert.Empty(test, f)
}

func TestFilesUnmarshalJSONInvalid(test *testing.T) {
	var f file.Files

	assert.Error(test, json.Unmarshal([]byte(`true`), &f))
	assert.Empty(test, f)
}
