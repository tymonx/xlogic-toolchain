// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package kind

import (
	"strings"

	"gitlab.com/tymonx/xlogic-toolchain/path"
)

// These constants define values for file.
const (
	Unknown       Kind = 0
	Object        Kind = 1
	Archive       Kind = 2
	Library       Kind = 3
	Executable    Kind = 4
	C             Kind = 5
	IP            Kind = 6
	Cxx           Kind = 7
	TCL           Kind = 8
	JSON          Kind = 9
	VHDL          Kind = 10
	Verilog       Kind = 11
	SystemVerilog Kind = 12
)

var gToString = map[Kind]string{ // nolint: gochecknoglobals
	Unknown:       "",
	Object:        "object",
	Archive:       "archive",
	Library:       "library",
	Executable:    "executable",
	C:             "c",
	IP:            "ip",
	Cxx:           "c++",
	TCL:           "tcl",
	JSON:          "json",
	VHDL:          "vhdl",
	Verilog:       "verilog",
	SystemVerilog: "systemverilog",
}

var gFromString = map[string]Kind{ // nolint: gochecknoglobals
	"":              Unknown,
	"object":        Object,
	"archive":       Archive,
	"library":       Library,
	"executable":    Executable,
	"c":             C,
	"ip":            IP,
	"c++":           Cxx,
	"tcl":           TCL,
	"json":          JSON,
	"vhdl":          VHDL,
	"verilog":       Verilog,
	"systemverilog": SystemVerilog,
}

var gFromExtension = map[string]Kind{ // nolint: gochecknoglobals
	".c":    C,
	".o":    Object,
	".a":    Archive,
	".lib":  Archive,
	".so":   Library,
	".dll":  Library,
	".exe":  Executable,
	".ip":   IP,
	".C":    Cxx,
	".cc":   Cxx,
	".cp":   Cxx,
	".cxx":  Cxx,
	".cpp":  Cxx,
	".c++":  Cxx,
	".tcl":  TCL,
	".vhd":  VHDL,
	".vhdl": VHDL,
	".json": JSON,
	".v":    Verilog,
	".sv":   SystemVerilog,
}

// Kind defines file kind.
type Kind int

// Is returns true if path is a file, otherwise it returns false.
func Is(p path.Path) bool {
	return fromExtension(p) != Unknown
}

// FromString returns kind from given string.
func FromString(str string) Kind {
	return gFromString[strings.TrimSpace(strings.ToLower(str))]
}

// FromPath returns kind from given file path.
func FromPath(p path.Path) Kind {
	return fromExtension(p)
}

// String returns kind as string.
func (k *Kind) String() string {
	return gToString[*k]
}

// UnmarshalText decodes from text to kind.
func (k *Kind) UnmarshalText(data []byte) error {
	*k = FromString(string(data))
	return nil
}

// MarshalText decodes from kind to text.
func (k *Kind) MarshalText() (data []byte, err error) {
	return []byte(k.String()), nil
}

func fromExtension(p path.Path) Kind {
	extension := p.Extension()

	if k, ok := gFromExtension[extension]; ok {
		return k
	}

	return gFromExtension[strings.ToLower(extension)]
}
