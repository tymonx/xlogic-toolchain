// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package file

import (
	"encoding/json"
	"io/ioutil"

	"gitlab.com/tymonx/go-error/rterror"
	"gitlab.com/tymonx/xlogic-toolchain/define"
	"gitlab.com/tymonx/xlogic-toolchain/file/kind"
	"gitlab.com/tymonx/xlogic-toolchain/header"
	"gitlab.com/tymonx/xlogic-toolchain/id"
	"gitlab.com/tymonx/xlogic-toolchain/include"
	"gitlab.com/tymonx/xlogic-toolchain/path"
	"gitlab.com/tymonx/xlogic-toolchain/undefine"
)

// Marshal is used only in testing and mocking.
var Marshal = json.Marshal // nolint: gochecknoglobals

// File defines a generic object that describes input or generated output file.
type File struct {
	ID        id.ID              `json:"id"`
	Kind      kind.Kind          `json:"kind"`
	Path      path.Path          `json:"path"`
	Compilers []string           `json:"compiler,omitempty"`
	Headers   header.Headers     `json:"header"`
	Defines   define.Defines     `json:"define"`
	Includes  include.Includes   `json:"include"`
	Undefines undefine.Undefines `json:"undefine"`
	Files     Files              `json:"file"`
}

// New creates a new file object.
func New(p path.Path) *File {
	return &File{
		ID:   id.New(),
		Kind: kind.FromPath(p),
		Path: p,
	}
}

// Load loads file content.
func (f *File) Load(p path.Path) (err error) {
	if k := kind.FromPath(p); (k != kind.JSON) && (k != kind.Unknown) {
		f.ID = id.New()
		f.Kind = k
		f.Path = p

		return nil
	}

	var data []byte

	if data, err = ioutil.ReadFile(p.String()); err != nil {
		return rterror.New("cannot read file", p, err)
	}

	if err := json.Unmarshal(data, f); err != nil {
		return rterror.New("cannot decode JSON to file", err)
	}

	return nil
}

// Dump dumps file content.
func (f *File) Dump(p path.Path) (err error) {
	var data []byte

	if data, err = Marshal(f); err != nil {
		return rterror.New("cannot encode file to JSON", err)
	}

	if err := ioutil.WriteFile(string(p), data, 0600); err != nil {
		return rterror.New("cannot write to file", p, err)
	}

	return nil
}

// UnmarshalText decodes text to file.
func (f *File) UnmarshalText(data []byte) error {
	return f.Load(path.Path(data))
}

// UnmarshalJSON decides JSON to file.
func (f *File) UnmarshalJSON(data []byte) error {
	switch data[0] {
	case '{':
	case '"':
		return f.UnmarshalText(data[1 : len(data)-1])
	case 'n':
		*f = File{}
		return nil
	default:
		return rterror.New("invalid JSON type")
	}

	object := new(struct {
		ID        id.ID              `json:"id"`
		Kind      kind.Kind          `json:"kind"`
		Path      path.Path          `json:"path"`
		Compilers []string           `json:"compiler,omitempty"`
		Headers   header.Headers     `json:"header"`
		Defines   define.Defines     `json:"define"`
		Includes  include.Includes   `json:"include"`
		Undefines undefine.Undefines `json:"undefine"`
		Files     Files              `json:"file"`
	})

	if err := json.Unmarshal(data, object); err != nil {
		return rterror.New("error occurred while decoding JSON", err)
	}

	*f = *object

	if f.Kind == kind.Unknown {
		f.Kind = kind.FromPath(f.Path)
	}

	return nil
}
