// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package define

import (
	"encoding/json"

	"gitlab.com/tymonx/go-error/rterror"
)

// Defines defines a list of defines.
type Defines []Define

// UnmarshalText decodes text to define.
func (d *Defines) UnmarshalText(data []byte) error {
	*d = Defines{*New(string(data))}

	return nil
}

// UnmarshalJSON decodes JSON to define.
func (d *Defines) UnmarshalJSON(data []byte) error {
	switch data[0] {
	case '[':
	case '{':
		object := new(Define)

		if err := json.Unmarshal(data, object); err != nil {
			return err
		}

		*d = Defines{*object}

		return nil
	case '"':
		return d.UnmarshalText(data[1 : len(data)-1])
	case 'n':
		*d = Defines{}
		return nil
	default:
		return rterror.New("invalid JSON type")
	}

	defines := []Define{}

	if err := json.Unmarshal(data, &defines); err != nil {
		return rterror.New("error occurred while decoding JSON", err)
	}

	*d = defines

	return nil
}
