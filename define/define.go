// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package define

import (
	"encoding/json"
	"strings"

	"gitlab.com/tymonx/go-error/rterror"
)

// Define defines define.
type Define struct {
	Name  string `json:"name,omitempty"`
	Value string `json:"value,omitempty"`
}

// New creates a new define object.
func New(value string) *Define {
	split := strings.SplitN(value, "=", 2)

	d := &Define{
		Name: strings.TrimSpace(split[0]),
	}

	if len(split) > 1 {
		d.Value = strings.TrimSpace(split[1])
	}

	return d
}

// String returns define as string.
func (d *Define) String() string {
	if d.Value == "" {
		return d.Name
	}

	return d.Name + "=" + d.Value
}

// UnmarshalText decodes text to define.
func (d *Define) UnmarshalText(data []byte) error {
	*d = *New(string(data))
	return nil
}

// UnmarshalJSON decodes JSON to define.
func (d *Define) UnmarshalJSON(data []byte) error {
	switch data[0] {
	case '{':
	case '"':
		return d.UnmarshalText(data[1 : len(data)-1])
	case 'n':
		*d = Define{}
		return nil
	default:
		return rterror.New("invalid JSON type")
	}

	object := new(struct {
		Name  string `json:"name"`
		Value string `json:"value"`
	})

	if err := json.Unmarshal(data, object); err != nil {
		return rterror.New("error occurred while decoding JSON", err)
	}

	d.Name = strings.TrimSpace(object.Name)
	d.Value = strings.TrimSpace(object.Value)

	return nil
}
