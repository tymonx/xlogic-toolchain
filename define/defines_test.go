// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package define_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/define"
)

func TestDefinesUnmarshalText(test *testing.T) {
	want := "DEFF=6"

	var d define.Defines

	assert.NoError(test, json.Unmarshal([]byte(` "`+want+`"  `), &d))

	assert.Len(test, d, 1)
	assert.Equal(test, "DEFF", d[0].Name)
	assert.Equal(test, "6", d[0].Value)
	assert.Equal(test, want, d[0].String())
}

func TestDefinesUnmarshalJSON(test *testing.T) {
	var d define.Defines

	assert.NoError(test, json.Unmarshal([]byte(` ["A=1", { "name": "B"  }]`), &d))

	assert.Len(test, d, 2)

	assert.Equal(test, "A", d[0].Name)
	assert.Equal(test, "1", d[0].Value)
	assert.Equal(test, "A=1", d[0].String())

	assert.Equal(test, "B", d[1].Name)
	assert.Empty(test, d[1].Value)
	assert.Equal(test, "B", d[1].String())
}

func TestDefinesUnmarshalJSONObject(test *testing.T) {
	want := "C"

	var d define.Defines

	assert.NoError(test, json.Unmarshal([]byte(` { "name": "`+want+`" }  `), &d))

	assert.Len(test, d, 1)
	assert.Equal(test, "C", d[0].String())
	assert.Equal(test, "C", d[0].Name)
	assert.Empty(test, d[0].Value)
}

func TestDefinesUnmarshalJSONNull(test *testing.T) {
	var d define.Defines

	assert.NoError(test, json.Unmarshal([]byte(` null  `), &d))
	assert.Empty(test, d)
}

func TestDefinesUnmarshalJSONError(test *testing.T) {
	var d define.Defines

	assert.Error(test, json.Unmarshal([]byte(`[5]`), &d))
	assert.Empty(test, d)
}

func TestDefinesUnmarshalJSONObjectError(test *testing.T) {
	var d define.Defines

	assert.Error(test, json.Unmarshal([]byte(`{"name":5}`), &d))
	assert.Empty(test, d)
}

func TestDefinesUnmarshalJSONInvalid(test *testing.T) {
	var d define.Defines

	assert.Error(test, json.Unmarshal([]byte(`true`), &d))
	assert.Empty(test, d)
}
