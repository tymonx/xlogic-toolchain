// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package define_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/define"
)

func TestDefineNew(test *testing.T) {
	want := "Name=value"

	d := define.New(want)

	assert.NotNil(test, d)
	assert.Equal(test, "Name", d.Name)
	assert.Equal(test, "value", d.Value)
	assert.Equal(test, want, d.String())
}

func TestDefineString(test *testing.T) {
	want := "NAME=5"

	assert.NotNil(test, want, define.New(want).String())
}

func TestDefineUnmarshalText(test *testing.T) {
	want := " MYDEF = text  "

	d := new(define.Define)

	assert.NoError(test, json.Unmarshal([]byte(`  "`+want+`"  `), d))

	assert.Equal(test, "MYDEF=text", d.String())
	assert.Equal(test, "MYDEF", d.Name)
	assert.Equal(test, "text", d.Value)
}

func TestDefineUnmarshalJSONObject(test *testing.T) {
	want := "DEFINE"

	d := new(define.Define)

	assert.NoError(test, json.Unmarshal([]byte(` { "name": "`+want+`" }  `), d))

	assert.Equal(test, want, d.String())
	assert.Equal(test, want, d.Name)
	assert.Empty(test, d.Value)
}

func TestDefineUnmarshalJSONNull(test *testing.T) {
	d := new(define.Define)

	assert.NoError(test, json.Unmarshal([]byte(` null  `), d))

	assert.Empty(test, d.String())
	assert.Empty(test, d.Name)
	assert.Empty(test, d.Value)
}

func TestDefineUnmarshalJSONError(test *testing.T) {
	d := new(define.Define)

	assert.Error(test, json.Unmarshal([]byte(`{"name":5}`), d))

	assert.Empty(test, d.String())
	assert.Empty(test, d.Name)
	assert.Empty(test, d.Value)
}

func TestDefineUnmarshalJSONInvalid(test *testing.T) {
	d := new(define.Define)

	assert.Error(test, json.Unmarshal([]byte(`true`), d))

	assert.Empty(test, d.String())
	assert.Empty(test, d.Name)
	assert.Empty(test, d.Value)
}
