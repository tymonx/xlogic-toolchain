// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package kind

import (
	"strings"

	"gitlab.com/tymonx/xlogic-toolchain/path"
)

// These constants define values for header file.
const (
	Unknown       Kind = 0
	C             Kind = 1
	Cxx           Kind = 2
	Verilog       Kind = 3
	SystemVerilog Kind = 4
)

var gToString = map[Kind]string{ // nolint: gochecknoglobals
	Unknown:       "",
	C:             "c",
	Cxx:           "c++",
	Verilog:       "verilog",
	SystemVerilog: "systemverilog",
}

var gFromString = map[string]Kind{ // nolint: gochecknoglobals
	"":              Unknown,
	"c":             C,
	"c++":           Cxx,
	"verilog":       Verilog,
	"systemverilog": SystemVerilog,
}

var gFromExtension = map[string]Kind{ // nolint: gochecknoglobals
	".h":   C,
	".hh":  Cxx,
	".hpp": Cxx,
	".hxx": Cxx,
	".h++": Cxx,
	".vh":  Verilog,
	".svh": SystemVerilog,
}

// Kind defines header file kind.
type Kind int

// Is returns true if path is a header file, otherwise it returns false.
func Is(file path.Path) bool {
	return gFromExtension[strings.ToLower(file.Extension())] != Unknown
}

// FromString returns kind from given string.
func FromString(str string) Kind {
	return gFromString[strings.TrimSpace(strings.ToLower(str))]
}

// FromPath returns kind from given file path.
func FromPath(file path.Path) Kind {
	return gFromExtension[strings.ToLower(file.Extension())]
}

// String returns kind as string.
func (k *Kind) String() string {
	return gToString[*k]
}

// UnmarshalText decodes from text to kind.
func (k *Kind) UnmarshalText(data []byte) error {
	*k = FromString(string(data))
	return nil
}

// MarshalText decodes from kind to text.
func (k *Kind) MarshalText() (data []byte, err error) {
	return []byte(k.String()), nil
}
