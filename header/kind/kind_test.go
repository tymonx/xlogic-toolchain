// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package kind_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/tymonx/xlogic-toolchain/header/kind"
)

func TestKindFromString(test *testing.T) {
	assert.Equal(test, kind.C, kind.FromString("c "))
	assert.Equal(test, kind.Cxx, kind.FromString("C++"))
	assert.Equal(test, kind.Verilog, kind.FromString("  VeRILOG"))
	assert.Equal(test, kind.SystemVerilog, kind.FromString("  SystemVERILOG "))
}

func TestKindFromPath(test *testing.T) {
	assert.Equal(test, kind.C, kind.FromPath("dir/file.h"))
	assert.Equal(test, kind.Cxx, kind.FromPath("dir/file.hh"))
	assert.Equal(test, kind.Cxx, kind.FromPath("dir/file.hpp"))
	assert.Equal(test, kind.Cxx, kind.FromPath("dir/file.hxx"))
	assert.Equal(test, kind.Cxx, kind.FromPath("dir/file.h++"))
	assert.Equal(test, kind.Verilog, kind.FromPath("dir/file.Vh"))
	assert.Equal(test, kind.SystemVerilog, kind.FromPath("dir/file.sVh"))
}

func TestKindIs(test *testing.T) {
	assert.True(test, kind.Is("dir/file.svh"))
	assert.False(test, kind.Is("dir/file.sv"))
}

func TestKindString(test *testing.T) {
	k := kind.FromPath("dir/file.svh")

	assert.Equal(test, "systemverilog", k.String())
}

func TestKindUnmarshalJSON(test *testing.T) {
	var k kind.Kind

	assert.NoError(test, json.Unmarshal([]byte(` " SystemVerilOg  " `), &k))
	assert.Equal(test, kind.SystemVerilog, k)
}

func TestKindMarshalJSON(test *testing.T) {
	k := kind.FromPath("dir/file.vh")

	data, err := json.Marshal(&k)

	assert.NoError(test, err)
	assert.Equal(test, []byte(`"verilog"`), data)
}
