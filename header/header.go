// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package header

import (
	"encoding/json"

	"gitlab.com/tymonx/go-error/rterror"
	"gitlab.com/tymonx/xlogic-toolchain/header/kind"
	"gitlab.com/tymonx/xlogic-toolchain/path"
)

// Header defines single header file.
type Header struct {
	Kind kind.Kind `json:"kind,omitempty"`
	Path path.Path `json:"path,omitempty"`
}

// Is returns true if path is a header file, otherwise it returns false.
func Is(file path.Path) bool {
	return kind.Is(file)
}

// New creates a new header file object from given file path.
func New(file path.Path) *Header {
	return &Header{
		Kind: kind.FromPath(file),
		Path: file,
	}
}

// String returns path string.
func (h *Header) String() string {
	return h.Path.String()
}

// UnmarshalText decodes text to header file path.
func (h *Header) UnmarshalText(data []byte) error {
	h.Path = path.Path(data)
	h.Kind = kind.FromPath(h.Path)

	return nil
}

// UnmarshalJSON decodes JSON to header file path.
func (h *Header) UnmarshalJSON(data []byte) error {
	switch data[0] {
	case '{':
	case '"':
		return h.UnmarshalText(data[1 : len(data)-1])
	case 'n':
		*h = Header{}
		return nil
	default:
		return rterror.New("invalid JSON type")
	}

	object := new(struct {
		Kind kind.Kind `json:"kind"`
		Path path.Path `json:"path"`
	})

	if err := json.Unmarshal(data, object); err != nil {
		return rterror.New("error occurred while decoding JSON", err)
	}

	h.Kind = object.Kind
	h.Path = object.Path

	if h.Kind == kind.Unknown {
		h.Kind = kind.FromPath(h.Path)
	}

	return nil
}
