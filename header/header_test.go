// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package header_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/header"
	"gitlab.com/tymonx/xlogic-toolchain/header/kind"
	"gitlab.com/tymonx/xlogic-toolchain/path"
)

func TestHeaderNew(test *testing.T) {
	want := "file.h"

	h := header.New(path.Path(want))

	assert.NotNil(test, h)
	assert.Equal(test, kind.C, h.Kind)
	assert.Equal(test, "c", h.Kind.String())
	assert.Equal(test, want, h.Path.String())
}

func TestHeaderString(test *testing.T) {
	want := "file.svh"

	assert.NotNil(test, want, header.New(path.Path(want)).String())
}

func TestHeaderIs(test *testing.T) {
	assert.True(test, header.Is("dir/file.hpp"))
	assert.False(test, header.Is("/dir/dir"))
}

func TestHeaderUnmarshalText(test *testing.T) {
	want := "dir/file.svh"

	h := new(header.Header)

	assert.NoError(test, json.Unmarshal([]byte(`  "`+want+`"  `), h))

	assert.Equal(test, want, h.Path.String())
	assert.Equal(test, kind.SystemVerilog, h.Kind)
	assert.Equal(test, "systemverilog", h.Kind.String())
}

func TestHeaderUnmarshalJSONObject(test *testing.T) {
	want := "dir/file.h++"

	h := new(header.Header)

	assert.NoError(test, json.Unmarshal([]byte(` { "path": "`+want+`" }  `), h))

	assert.Equal(test, want, h.Path.String())
	assert.Equal(test, kind.Cxx, h.Kind)
	assert.Equal(test, "c++", h.Kind.String())
}

func TestHeaderUnmarshalJSONNull(test *testing.T) {
	h := new(header.Header)

	assert.NoError(test, json.Unmarshal([]byte(` null  `), h))

	assert.Empty(test, h.Path.String())
	assert.Empty(test, h.Kind.String())
	assert.Equal(test, kind.Unknown, h.Kind)
}

func TestHeaderUnmarshalJSONError(test *testing.T) {
	h := new(header.Header)

	assert.Error(test, json.Unmarshal([]byte(`{"path":5}`), h))

	assert.Empty(test, h.Path.String())
	assert.Empty(test, h.Kind.String())
	assert.Equal(test, kind.Unknown, h.Kind)
}

func TestHeaderUnmarshalJSONInvalid(test *testing.T) {
	h := new(header.Header)

	assert.Error(test, json.Unmarshal([]byte(`true`), h))

	assert.Empty(test, h.Path.String())
	assert.Empty(test, h.Kind.String())
	assert.Equal(test, kind.Unknown, h.Kind)
}
