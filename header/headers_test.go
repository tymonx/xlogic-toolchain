// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package header_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tymonx/xlogic-toolchain/header"
	"gitlab.com/tymonx/xlogic-toolchain/header/kind"
)

func TestHeadersUnmarshalText(test *testing.T) {
	want := "dir/file.hpp"

	var h header.Headers

	assert.NoError(test, json.Unmarshal([]byte(` "`+want+`"  `), &h))

	assert.Len(test, h, 1)
	assert.Equal(test, kind.Cxx, h[0].Kind)
	assert.Equal(test, "c++", h[0].Kind.String())
	assert.Equal(test, want, h[0].Path.String())
}

func TestHeadersUnmarshalJSON(test *testing.T) {
	var h header.Headers

	assert.NoError(test, json.Unmarshal([]byte(` ["dir/file.h", { "path": "file.svh"  }]`), &h))

	assert.Len(test, h, 2)

	assert.Equal(test, kind.C, h[0].Kind)
	assert.Equal(test, "c", h[0].Kind.String())
	assert.Equal(test, "dir/file.h", h[0].Path.String())

	assert.Equal(test, kind.SystemVerilog, h[1].Kind)
	assert.Equal(test, "systemverilog", h[1].Kind.String())
	assert.Equal(test, "file.svh", h[1].Path.String())
}

func TestHeadersUnmarshalJSONObject(test *testing.T) {
	want := "dir/file.vh"

	var h header.Headers

	assert.NoError(test, json.Unmarshal([]byte(` { "path": "`+want+`" }  `), &h))

	assert.Len(test, h, 1)
	assert.Equal(test, want, h[0].Path.String())
	assert.Equal(test, kind.Verilog, h[0].Kind)
	assert.Equal(test, "verilog", h[0].Kind.String())
}

func TestHeadersUnmarshalJSONNull(test *testing.T) {
	var h header.Headers

	assert.NoError(test, json.Unmarshal([]byte(` null  `), &h))
	assert.Empty(test, h)
}

func TestHeadersUnmarshalJSONError(test *testing.T) {
	var h header.Headers

	assert.Error(test, json.Unmarshal([]byte(`[5]`), &h))
	assert.Empty(test, h)
}

func TestHeadersUnmarshalJSONObjectError(test *testing.T) {
	var h header.Headers

	assert.Error(test, json.Unmarshal([]byte(`{"path":5}`), &h))
	assert.Empty(test, h)
}

func TestHeadersUnmarshalJSONInvalid(test *testing.T) {
	var h header.Headers

	assert.Error(test, json.Unmarshal([]byte(`true`), &h))
	assert.Empty(test, h)
}
