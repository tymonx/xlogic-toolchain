// Copyright 2020 Tymoteusz Blazejczyk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package header

import (
	"encoding/json"

	"gitlab.com/tymonx/go-error/rterror"
	"gitlab.com/tymonx/xlogic-toolchain/path"
)

// Headers defines a list of header files.
type Headers []Header

// UnmarshalText decodes text to header file path.
func (h *Headers) UnmarshalText(data []byte) error {
	*h = Headers{*New(path.Path(data))}

	return nil
}

// UnmarshalJSON decodes JSON to header file path.
func (h *Headers) UnmarshalJSON(data []byte) error {
	switch data[0] {
	case '[':
	case '{':
		object := new(Header)

		if err := json.Unmarshal(data, object); err != nil {
			return err
		}

		*h = Headers{*object}

		return nil
	case '"':
		return h.UnmarshalText(data[1 : len(data)-1])
	case 'n':
		*h = Headers{}
		return nil
	default:
		return rterror.New("invalid JSON type")
	}

	headers := []Header{}

	if err := json.Unmarshal(data, &headers); err != nil {
		return rterror.New("error occurred while decoding JSON", err)
	}

	*h = headers

	return nil
}
